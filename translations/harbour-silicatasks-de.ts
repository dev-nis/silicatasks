<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>CoverPage</name>
    <message>
        <source>My Cover</source>
        <translation>Mein Cover</translation>
    </message>
</context>
<context>
    <name>LicenseListPart</name>
    <message>
        <source>License text</source>
        <translation>Lizenztext</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Create new list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>My lists</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Deleting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>About</source>
        <translation type="unfinished">Über</translation>
    </message>
</context>
<context>
    <name>NewListDialog</name>
    <message>
        <source>Create new list</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Opal.About</name>
    <message>
        <source>About</source>
        <translation>Über</translation>
    </message>
    <message>
        <source>Version %1</source>
        <translation>Version %1</translation>
    </message>
    <message>
        <source>Development</source>
        <translation>Entwicklung</translation>
    </message>
    <message>
        <source>show contributors</source>
        <translation>Mitwirkende zeigen</translation>
    </message>
    <message>
        <source>Homepage</source>
        <translation>Webseite</translation>
    </message>
    <message>
        <source>Changelog</source>
        <translation>Änderungsverlauf</translation>
    </message>
    <message>
        <source>Translations</source>
        <translation>Übersetzungen</translation>
    </message>
    <message>
        <source>Source Code</source>
        <translation>Quellcode</translation>
    </message>
    <message>
        <source>Donations</source>
        <translation>Spenden</translation>
    </message>
    <message>
        <source>License</source>
        <translation>Lizenz</translation>
    </message>
    <message numerus="yes">
        <source>show license(s)</source>
        <translation>
            <numerusform>Lizenz zeigen</numerusform>
        </translation>
    </message>
    <message>
        <source>Contributors</source>
        <translation>Mitwirkende</translation>
    </message>
    <message>
        <source>Acknowledgements</source>
        <translation>Danksagungen</translation>
    </message>
    <message>
        <source>Thank you!</source>
        <translation>Vielen Dank!</translation>
    </message>
    <message>
        <source>Details</source>
        <translation>Details</translation>
    </message>
    <message>
        <source>show details</source>
        <translation>Details zeigen</translation>
    </message>
    <message>
        <source>Download license texts</source>
        <translation>Lizenztexte herunterladen</translation>
    </message>
    <message numerus="yes">
        <source>License(s)</source>
        <translation>
            <numerusform>Lizenz</numerusform>
        </translation>
    </message>
    <message>
        <source>Note: please check the source code for most accurate information.</source>
        <translation>Hinweis: Bitte prüfen Sie den Quellcode für alle Einzelheiten.</translation>
    </message>
    <message>
        <source>External Link</source>
        <translation>Externer Link</translation>
    </message>
    <message>
        <source>Open in browser</source>
        <translation>Im Browser öffnen</translation>
    </message>
    <message>
        <source>Copied to clipboard: %1</source>
        <translation>In die Zwischenablage kopiert: %1</translation>
    </message>
    <message>
        <source>Copy to clipboard</source>
        <translation>In die Zwischenablage kopieren</translation>
    </message>
    <message>
        <source>Please refer to &lt;a href=&quot;%1&quot;&gt;%1&lt;/a&gt;</source>
        <translation>Bitte beachten Sie &lt;a href=&quot;%1&quot;&gt;%1&lt;/a&gt;</translation>
    </message>
    <message>
        <source>News</source>
        <translation>Neuigkeiten</translation>
    </message>
    <message>
        <source>Changes since version %1</source>
        <translation>Änderungen seit Version %1</translation>
    </message>
</context>
<context>
    <name>Opal.About.Common</name>
    <message>
        <source>If you want to support my work, you can buy me a cup of coffee.</source>
        <translation>Sie können mir gerne einen Kaffee spendieren, wenn Sie meine Arbeit unterstützen möchten.</translation>
    </message>
    <message>
        <source>You can support this project by contributing, or by donating using any of these services.</source>
        <translation>Sie können dieses Projekt durch Ihre Mitarbeit oder durch eine Spende über einen dieser Dienste unterstützen.</translation>
    </message>
    <message>
        <source>Your contributions to translations or code would be most welcome.</source>
        <translation>Ihre Mitarbeit bei Übersetzungen oder der Programmierung wäre eine große Hilfe.</translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Item</source>
        <translation type="unfinished">Element</translation>
    </message>
</context>
</TS>
