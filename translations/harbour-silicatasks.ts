<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>CoverPage</name>
    <message>
        <source>My Cover</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LicenseListPart</name>
    <message>
        <source>License text</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Create new list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>My lists</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Deleting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NewListDialog</name>
    <message>
        <source>Create new list</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Opal.About</name>
    <message>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Version %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Development</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>show contributors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Homepage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Changelog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Translations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Source Code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Donations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>License</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <source>show license(s)</source>
        <translation type="unfinished">
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <source>News</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Changes since version %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>show details</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Contributors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Acknowledgements</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Thank you!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Details</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>External Link</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Open in browser</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Copied to clipboard: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Copy to clipboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please refer to &lt;a href=&quot;%1&quot;&gt;%1&lt;/a&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Download license texts</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <source>License(s)</source>
        <translation type="unfinished">
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <source>Note: please check the source code for most accurate information.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Opal.About.Common</name>
    <message>
        <source>If you want to support my work, you can buy me a cup of coffee.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>You can support this project by contributing, or by donating using any of these services.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Your contributions to translations or code would be most welcome.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Item</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
