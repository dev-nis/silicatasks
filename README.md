# SilicaTasks
An App for SailfishOS that provides a simple To-Do List with reminders as notifications

## Description
With this SailfishOS-App you will be able to create a To-Do list with multiple items. For everyone of these items you will be able to set a due date. If this date arrives, the app will remind you, it will send you a notification. In the future, it may also be possible to have multiple To-Do lists.

## Installation
Once a first release is out, you will be able to install the app from OpenRepos (and hopefully the Jolla Store)

## Roadmap
The first goal is to get one simple To-Do list with notifications working. Once thats done, I plan to add the possibility to have multiple lists. I might also add CalDav synchronisation at a later point, if I figure out how to do so.

## License
I don't know much about all of the OpenSource-licenses

## Project status
Currently in development, but I'm still learning the concepts of SailfishOS App-development, so it could take a while
