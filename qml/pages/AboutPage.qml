import QtQuick 2.0
import Sailfish.Silica 1.0
import "../modules/Opal/About"
import "../modules/Opal/Attributions"

AboutPageBase
{
    id: root
    allowedOrientations: Orientation.All
    appName: "SilicaTasks"
    appIcon: Qt.resolvedUrl("../images/harbour-silicatasks.png")
    appVersion: "1.0.0"
    description: "An App for SailfishOS that provides a simple To-Do List with reminders as notifications."
    authors: "2024 NSC IT Solutions (NIS)"
    licenses: License { spdxId: "GPL-3.0-or-later" }
    attributions: OpalAboutAttribution {}
    sourcesUrl: "https://gitlab.com/dev-nis/silicatasks"
}
