import QtQuick 2.0
import Sailfish.Silica 1.0
import "../"

Page
{
    id: page

    // The effective value will be restricted by ApplicationWindow.allowedOrientations
    allowedOrientations: Orientation.All

    // To enable PullDownMenu, place our content in a SilicaFlickable
    SilicaFlickable
    {
        anchors.fill: parent

        // PullDownMenu and PushUpMenu must be declared in SilicaFlickable, SilicaListView or SilicaGridView
        PullDownMenu
        {
            MenuItem
            {
                text: qsTr("About")
                onClicked: pageStack.animatorPush(Qt.resolvedUrl("AboutPage.qml"))
            }
            MenuItem
            {
                text: qsTr("Settings")
                onClicked: pageStack.animatorPush(Qt.resolvedUrl("SettingsPage.qml"))
            }
            MenuItem
            {
                text: qsTr("Create new list")
                onClicked: {
                    //pageStack.animatorPush(Qt.resolvedUrl("NewListDialog.qml"))
                    var dialog = pageStack.push(Qt.resolvedUrl("NewListDialog.qml"))
                    dialog.accepted.connect(function()
                    {
                        //header.title = "My name: "
                        console.log("New list is: " + dialog.name)
                    })
                }
            }
        }

        // Tell SilicaFlickable the height of its content.
        contentHeight: column.height

        // Place our content in a Column.  The PageHeader is always placed at the top
        // of the page, followed by our content.
        Column
        {
            id: column
            width: page.width
            spacing: Theme.paddingLarge

            PageHeader
            {
                id: header
                title: qsTr("My lists")
            }

            SilicaListView
            {
                id: taskList
                width: page.width
                anchors.top: header.bottom
                spacing: Theme.horizontalPageMargin

                model: ListModel
                {
                    id: listModel
                    ListElement
                    {
                        listName: "Default List"
                    }
                }

                delegate: ListItem
                {
                    id: listItem
                    width: ListView.view.width

                    MainPageButton
                    {
                        text: listName
                        /*onClicked: pageStack.push(...
                                                  */
                    }

                    RemorseItem
                    {
                        id: remorse
                    }

                     function showRemorseItem()
                     {
                         var idx = model.index
                         remorse.execute(listItem, qsTr("Deleting"), function() { listModel.remove(idx) } )
                     }

                    menu: ContextMenu
                    {
                        MenuItem
                        {
                            text: qsTr("Remove")
                            onClicked: showRemorseItem()
                        }
                    }
                }
            }
        }
    }
}
