import QtQuick 2.2
import Sailfish.Silica 1.0

Dialog
{
    property string name

    Column
    {
        width: parent.width

        DialogHeader
        {
            title: qsTr("Create new list")
        }

        TextField
        {
            id: nameField
            width: parent.width
            placeholderText: "Name of the list"
            label: "Name"
        }
    }

    onDone:
    {
        if (result == DialogResult.Accepted)
        {
            name = nameField.text
        }
    }
}
